"use strict";

docReady(() => {

	if (window.innerWidth > 576) {
		console.log(window.innerWidth);

		const timelines = document.querySelectorAll('.timeline');
		timelines.forEach(timeline => {
			const tm = timeline.querySelector(".timeline-slider-container");
			const slides = tm.children;
			let slide = 0;
			const slideMax = slides.length - 1;
			const dateContainer = timeline.querySelector(".timeline-date-back");
			const sWidth = window.innerWidth;

			const prev = timeline.querySelector("#timeline-prev");
			const next = timeline.querySelector("#timeline-next");
			const line = timeline.querySelector("#timeline-line");

			if (sWidth > 768) ecart();
			slides[0].classList.add("slide-see");

			line.style.width =
				slides[0].offsetLeft + slides[0].offsetParent.offsetLeft + 16 + "px";

			newDate(dateContainer.querySelector("div").innerText, dateContainer);


			next.addEventListener("click", () => {
				if (slide < slideMax) {
					slide += 1;
					toSlide(slides, line, slide);
				}
			});

			prev.addEventListener("click", () => {
				if (slide > 0) {
					slide -= 1;
					toSlide(slides, line, slide);
				}
			});

			const dots = timeline.querySelectorAll(".slide-dot");
			dots.forEach((dot) => {
				dot.addEventListener("click", () => {
					;
					slide = parseInt(dot.dataset.slide);
					toSlide(slides, line, slide);
				});
			});

			window.addEventListener("resize", () => {
				if (sWidth > 768) ecart();
				toSlide(slides, line, slide);
			});

			function ecart() {
				for (let i = 0; i < slides.length; i++) {
					if (slides[i].offsetLeft + slides[i].querySelector(".cc").clientWidth > tm.clientWidth) {
						slides[i].classList.add("slide-right");
					}
				}
			}

			function toSlide(slides, line, nb) {
				let lineWidth = parseInt(line.style.width, 10);
				let newLineWith = slides[nb].offsetLeft + slides[nb].offsetParent.offsetLeft + 16;
				let diff = newLineWith - lineWidth;
				const animF = 0.95;

				if (diff < 0) {
					diff *= -1;
				}

				line.style.transition = `${diff * animF}ms all ease-out`;
				line.style.width = newLineWith + "px";

				for (let i = 0; i < slides.length; i++) {
					slides[i].classList.remove("slide-see");
				}
				slides[nb].classList.add("slide-see");

				newDate(slides[nb].children[0].innerText);
			}

			function newDate(date) {
				date = date.split("");

				dateContainer.innerHTML = "";
				let newDate = document.createElement("div");
				date.forEach((el) => {
					let newNum = document.createElement("span");
					newNum.innerHTML = el;
					dateContainer.append(newNum);
				});
			}


			// GIVE HEIGHT DINAMICLY
			setTimeout(function () {
				function getMaxHeight(maxheightf) {
					var max = 0;
					const ccTexts = timeline.querySelectorAll('.timeline-slide .cc');
					ccTexts.forEach(ccText => {
						console.info(Math.max(ccText.scrollHeight, max));
						if (ccText.scrollHeight > max) max = ccText.scrollHeight;
					}
					);
					return max;
				}
				var max = getMaxHeight();
				// SET HEIGHT
				timeline.style.height = max * 2 + "px";
			}, 500);
		});




	} else {
		let timelineContainer = document.querySelectorAll('.timeline-slide__ul')
		const timelineTrigger = () => {
			Array.from(timelineContainer).forEach(e => {
				e.animate([
					// étapes/keyframes
					{ transform: 'translateX(0px)' },
					{ transform: 'translateX(-30px)' },
					{ transform: 'translateX(-0px)' }
				], {
					delay: 1000,
					duration: 1500,
					easing: 'cubic-bezier(0.4, 0, 0.2, 1)'
				});
			});
		}
		ScrollTrigger.create({
			trigger: ".timeline-slide",
			onEnter: timelineTrigger,
		});
	}
});
