<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';


$nom = get_field('nom_modal');
$isMobile = get_field('timeline_visibility_mobile');

if(!wp_is_mobile()): ?>


<div id="<?= $id ?>" class="timeline <?= $css ?>">

    <div class="timeline-slider">

        <div class="timeline-slider-container">
            <?php
            if (have_rows('dates')) :
                while (have_rows('dates')) : the_row();

                    $date = get_sub_field('date');
                    $titre = get_sub_field('titre');
                    $description = get_sub_field('description');
                    $index = get_row_index();
            ?>

                    <div class="timeline-slide" >
                        <p class="slide-date"><?=  $date ?></p>
                        <span class="slide-dot anim-300" data-slide="<?=  $index-1 ?>"></span>
                        <div class="cc">
                            <h3 class="has-quotetitle-font-size m-0 fs-20 fw-700"><?=  $titre ?></h3>    
                            <p><?=  $description ?></p>
                        </div>
                    </div>


            <?php
                endwhile;
            endif;
            ?>
        </div>


        <div id="timeline-prev" class="d-flex position-absolute z-9 timeline-arrows">
        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.835 3.96995L16.065 2.19995L6.16504 12.1L16.065 22L17.835 20.2299L9.70504 12.1L17.835 3.96995V3.96995Z" fill="white"/>
        </svg>
        </div>

        <div id="timeline-next" class="d-flex position-absolute z-9 timeline-arrows">
            <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><title>arrow_forward_ios</title><g fill="none" class="nc-icon-wrapper"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2 5.88 4.12z" fill="#FFF"></path></g></svg>
        </div>

        <div class="timeline-date-back">
            <div><?=  get_field('dates')[0]['date']; ?></div>
        </div>
        <span id="timeline-line"></span>


    </div>

</div>


<?php
    elseif(wp_is_mobile() && $isMobile):
        if (have_rows('dates')) :?>
            <div class="timeline-slide hide-scrollbar hide-scrollbar::-webkit-scrollbar">
                <?php
                    while (have_rows('dates')) : the_row();

                        $date = get_sub_field('date');
                        $titre = get_sub_field('titre');
                        $description = get_sub_field('description');
                        // $index = get_row_index();
                ?>

                        <ul class="timeline-slide__ul">
                            <li class="date">
                            <?=  $date ?>
                            </li>
                            <li class="title has-bigfont-font-size">
                            <?=  $titre ?>
                            </li>
                            <li>
                            <?=  $description ?>
                            </li>
                        </ul>
                <?php
                        endwhile;
            endif;
        ?>
            </div>
<?php
endif; ?>  